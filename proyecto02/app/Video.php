<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = ['category_id', 'description'];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
    public function Tag_video()
    {
    	return $this->hasMany('App\Tag_video');
    }
    public function Playlist_video()
    {
    	return $this->hasMany('App\Playlist_video');
    }
}
