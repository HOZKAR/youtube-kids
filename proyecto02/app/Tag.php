<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['description'];

    public function Tag_video()
    {
    	return $this->hasMany('App\Tag_video');
    }
}
