<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Children extends Model
{
    protected $fillable = ['user_id', 'username', 'password', 'birthdate', 'enable_search'];

    public function User()
    {
    	return $this->belongsTo('App\User');
    }
    public function Children_playlist()
    {
    	return $this->hasMany('App\Children_playlist');
    }
}
