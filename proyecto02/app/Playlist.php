<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $fillable = ['user_id', 'description'];

    public function User()
    {
    	return $this->belongsTo('App\User');
    }
    public function Children_playlist()
    {
    	return $this->hasMany('App\Children_playlist');
    }
    public function Playlist_video()
    {
    	return $this->hasMany('App\Playlist_video');
    }
}
