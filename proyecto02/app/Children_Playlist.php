<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Children_Playlist extends Model
{
    protected $fillable = ['children_id', 'playlist_id'];

    public function Children()
    {
    	return $this->belongsTo('App\Children');
    }
    public function Playlist()
    {
    	return $this->belongsTo('App\Playlist');
    }
}
