<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag_video extends Model
{
    protected $fillable = ['tag_id', 'video_id'];

    public function Tag()
    {
    	return $this->belongsTo('App\Tag');
    }

    public function Video()
    {
    	return $this->belongsTo('App\Video');
    }
}
