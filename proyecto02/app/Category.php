<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['description', 'name', 'minimum_age'];

    public function Video()
    {
    	return $this->hasMany('App\Video');
    }
}
