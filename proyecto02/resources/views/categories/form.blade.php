<!-- Nombre Form Input -->
<div class="form-group">
    {!! Form::label('name','Name: ')  !!}
    {!! Form::text('name', null , ['class'=>'form-control'])  !!}
</div>
<div class="form-group">
    {!! Form::label('description','Description: ')  !!}
    {!! Form::text('description', null , ['class'=>'form-control'])  !!}
</div>
<div class="form-group">
    {!! Form::label('minimum_age','Minimum_Age: ')  !!}
    {!! Form::text('minimun_age', null , ['class'=>'form-control'])  !!}
</div>

<!-- Submit -->
<div class="form-group">
    <!-- Submit -->
    <div class="form-group">
        <button type="submit" class="btn btn-primary">
            <span class="glyphicon glyphicon-save" aria-hidden="true">{{ ' '.$submitButtonText }}</span>
        </button>
        <a href="{{ url('categories/') }}" class="btn btn-default">
            <span class="glyphicon glyphicon-backward" aria-hidden="true"> Cancelar</span>
        </a>
    </div>
</div>