@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h1>Crear Categoría</h1>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            {!! Form::open(['url' => 'categories']) !!}
            @include('categories.form',['submitButtonText'=>'Crear Categoría'])
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    </div>

@stop