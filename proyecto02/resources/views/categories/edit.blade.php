@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h1>Editando: {!! $category->name  !!}</h1>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            {!! Form::model($category, ['method'=>'PATCH', 'action' => ['CategoriesController@update', $category->id]]) !!}
            @include('categories.form',['submitButtonText'=>'Actualizar categoria'])
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    </div>

@stop