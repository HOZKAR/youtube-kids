@extends('app')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h1>Detalle de la Categoria</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">

            <table class="table table-striped">
                <tr>
                    <td class="col-md-3"><strong>Nombre: </strong></td>
                    <td>{{ $category->name }}</td>
                </tr>
                <tr>
                    <td class="col-md-3"><strong>Descripcion: </strong></td>
                    <td>{{ $category->description }}</td>
                </tr>
                <tr>
                    <td class="col-md-3"><strong>Edad mínima: </strong></td>
                    <td>{{ $category->minimum_age }}</td>
                </tr>
            </table>

        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            {!! Form::open(['method'=>'DELETE', 'action' => ['CategoriesController@destroy', $category->id]]) !!}
            <!-- Submit -->
            <div class="form-group">
                <a href="{{ url('categories/'.$category->id.'/edit') }}" class="btn btn-primary">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"> Editar</span>
                </a>
                <button type="submit" class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"> Eliminar</span>
                </button>
                <a href="{{ url('categories') }}" class="btn btn-default">
                    <span class="glyphicon glyphicon-backward" aria-hidden="true"> Cancelar</span>
                </a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop