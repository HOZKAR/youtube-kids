@extends('app')

@section('content')

    {{-- Auth::user()->id  --}}

    <a class="btn btn-primary btn-lg" href="{{ url('categories/create') }}"
       role="button">Crear Categoria</a>
    <hr>

    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading">
            <h1>Categorias</h1>
        </div>
        @if ($categories->count())
            <!-- Table -->
            <table class="table table-striped col-md-12">
                <thead>
                    <tr>
                        <th class="text-center">Editar</th>
                        <th class="text-center">Ver</th>
                        <th>Nombre</th>
                        <th class="text-center">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td class="col-md-1 text-center">
                                <a href="{{ url('categories/'.$category->id.'/edit') }}">
                                    <button>
                                        <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                                    </button>
                                </a>
                            </td>
                            <td class="col-md-1 text-center">
                                <a href="{{ url('categories/'.$category->id) }}">
                                    <button>
                                        <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
                                    </button>
                                </a>
                            </td>
                            <td class="col-md-10">
                                {{ $category->name }}
                            </td>
                            <td class="col-md-1 text-center">
                                {!! Form::open(['method'=>'DELETE', 'action' => ['CategoriesController@destroy', $category->id]]) !!}
                                    <button type="submit"><i class="glyphicon glyphicon-trash"></i></button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div>
        {!! $categories->render() !!}
    </div>
    <br>
@stop