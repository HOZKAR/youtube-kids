<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
  "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <title>4kids</title>
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!!Html::style('assets/css/metisMenu.min.css')!!}
    {!!Html::style('assets/css/sb-admin-2.css')!!}
    {!!Html::style('assets/css/font-awesome.min.css')!!}
</head>
<body>
  <!-- Navbar -->
  @include('partials.laravelnavbar')
  
  <div class="container">
    @yield('content')
  </div>
  <div class="footer">
    @yield('footer')

    <!-- Scripts -->
    {!! Html::script('/js/jquery-1.11.3.min.js') !!}
    {!! Html::script('/js/bootstrap.min.js') !!}

  </div>
</html>
